import {Entity, model, property} from '@loopback/repository';

@model()
export class Rol extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: false,
    required: true,
  })
  id: number;

  @property({
    type: 'number',
    required: true,
  })
  idUsuario: number;

  @property({
    type: 'string',
  })
  rol?: string;

  @property({
    type: 'string',
  })
  descripcion?: string;


  constructor(data?: Partial<Rol>) {
    super(data);
  }
}

export interface RolRelations {
  // describe navigational properties here
}

export type RolWithRelations = Rol & RolRelations;
